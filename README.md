# TCGA #

Various tools and scripts for working with TCGA data. The docker image is designed to be used on protected data systems for running analysis on TCGA data.
